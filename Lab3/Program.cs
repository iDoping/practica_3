﻿using System;

namespace Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Исключение при делении на 0
            Console.WriteLine("Введите 2 числа:");
            int a = Int32.Parse(Console.ReadLine());
            int b = Int32.Parse(Console.ReadLine());
            try
            {
                Console.WriteLine($"Результат деления = {divMethod(a, b)}");
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Ошибка деления на 0");
            }
            //Исключение при выходе индекса за предлы массива
            int c = Int32.Parse(Console.ReadLine());
            int[] mass = new int[5] { 1, 2, 3, 4, 5 };
            int v = 0;
            try
            {
                v = mass[massind(c)];
                Console.WriteLine("Элемент массива: " + v);
            }
            catch (IndexOutOfRangeException)
            {
                Console.WriteLine("Индекс находится за пределами массива");
            }
            //Исключение при ошибке приведения типов
            bool flag = true;
            tips(flag);
        }

        private static void tips(bool v1) 
        {
            try
            {
                Char ch = Convert.ToChar(v1);
            }
            catch (InvalidCastException)
            {
                Console.WriteLine("Невозможно привести char к bool");
            }
        }

        private static int divMethod(int a, int b)
        {
            return a / b;
        }

        private static int massind(int a)
        {
            return a;
        }
    }
}